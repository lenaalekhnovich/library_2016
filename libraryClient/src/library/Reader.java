package library;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Босс on 28.09.2016.
 */
public class Reader implements Serializable {
    private String surname;
    private String name;
    private String lastname;
    private Date dateOfBirth;
    private String mobileNumber;

    public Reader(){
    }
    public Reader(String surname, String name, String lastname, Date dateOfBirth, String mobileNumber) {
        this.surname = surname;
        this.name = name;
        this.lastname = lastname;
        this.dateOfBirth = dateOfBirth;
        this.mobileNumber = mobileNumber;
    }

    public String getSurname() {
        return surname;
    }

    public String getName() {
        return name;
    }

    public String getLastname() {
        return lastname;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String toString(){
        String string = surname + " " + name + " " + lastname + " " + dateOfBirth + " " + mobileNumber;
        return string;
    }
}

