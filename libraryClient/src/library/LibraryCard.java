package library;

import java.io.Serializable;
import java.util.LinkedList;


/**
 * Created by Босс on 30.09.2016.
 */
public class LibraryCard implements Serializable {
    private String id;
    private LinkedList<Order> order;
    private Reader reader;

    public LibraryCard(){
        order = new LinkedList<>();
        reader = new Reader();
    }

    public Reader getReader() {
        return reader;
    }

    public String getId() {
        return id;
    }

    public LinkedList<Order> getOrder() {
        return order;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setReader(Reader reader) {
        this.reader = reader;
    }

    public void addOrder(Order order) {
        this.order.add(order);
    }

    public LibraryCard(LinkedList<Order> order, Reader reader) {
        order = new LinkedList<>();
        reader = new Reader();
        this.order = order;
        this.reader = reader;
    }

    @Override
    public String toString(){
        String string;
        string = reader.toString() + order.toString();
        return string;
    }
}

