package library;

import java.io.Serializable;

/**
 * Created by Босс on 28.09.2016.
 */
public class Book implements Serializable{
    private String author;
    private String name;
    private int yearPublish;
    private int amountInLibrary;
    private int amountTaken;
    private Publisher publisher;
    private KindOfBook kind;

    public Book(){}

    public Book(String author, String name, int yearPublish, int amountInLibrary) {
        this.author = author;
        this.name = name;
        this.yearPublish = yearPublish;
        this.amountInLibrary = amountInLibrary;
    }

    public Book(String author, String name, int yearPublish, int amountInLibrary, int amountTaken, Publisher publisher, KindOfBook kind) {
        this.author = author;
        this.name = name;
        this.yearPublish = yearPublish;
        this.amountInLibrary = amountInLibrary;
        this.amountTaken = amountTaken;
        this.publisher = publisher;
        this.kind = kind;
    }

    public String toString(){
        String string = kind.getNameKind()+ " " + author + " " + name +  " " + publisher.getName()+ " " + yearPublish ;
        return string;
    }

    public String getAuthor() {
        return author;
    }

    public String getName() {
        return name;
    }
    public int getYearPublish() {
        return yearPublish;
    }
    public int getAmountInLibrary() {
        return amountInLibrary;
    }
    public int getAmountTaken() {
        return amountTaken;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public KindOfBook getKind() {
        return kind;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setYearPublish(int yearPublish) {
        this.yearPublish = yearPublish;
    }
    public void setAmountInLibrary(int amountInLibrary) {
        this.amountInLibrary = amountInLibrary;
    }
    public void setAmountTaken(int amountTaken) {
        this.amountTaken = amountTaken;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;

        Book book = (Book) o;

        if (getYearPublish() != book.getYearPublish()) return false;
        if (getAmountInLibrary() != book.getAmountInLibrary()) return false;
        if (getAmountTaken() != book.getAmountTaken()) return false;
        if (!getAuthor().equals(book.getAuthor())) return false;
        if (!getName().equals(book.getName())) return false;
        if (!getPublisher().equals(book.getPublisher())) return false;
        return getKind().equals(book.getKind());

    }

    @Override
    public int hashCode() {
        int result = getAuthor().hashCode();
        result = 31 * result + getName().hashCode();
        result = 31 * result + getYearPublish();
        result = 31 * result + getAmountInLibrary();
        result = 31 * result + getAmountTaken();
        result = 31 * result + getPublisher().hashCode();
        result = 31 * result + getKind().hashCode();
        return result;
    }
}
