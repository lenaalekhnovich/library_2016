package library;

import java.io.Serializable;

/**
 * Created by Босс on 16.10.2016.
 */
public class Publisher implements Serializable {

    private String name;
    public String phone;

    public Publisher(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    @Override
    public String toString() {
        return "Publisher{" +
                "name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
