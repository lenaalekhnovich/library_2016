package client;

import connection.CheckThread;
import library.Reader;

import javax.swing.*;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;

/**
 * Created by Босс on 12.11.2016.
 */
public class GUIMainMenuAdmin extends JFrame {
    private Container PanelWhole;
    private JPanel PanelReader;
    private JPanel PanelProperties;

    private JButton buttonOrder;
    private JButton buttonFind;
    private JButton buttonAddReader;
    private JButton buttonRemoveReader;
    private JButton buttonExit;
    private JButton buttonListLate;
    private JButton buttonGraphic;
    private JButton buttonReportOrders;
    private JButton buttonReportReaders;

    private JTextField fieldFind;

    private TableModelReaders model;
    private JTable table;
    private JScrollPane scrollPane;

    public static int choise = 0;
    private static HashMap<Integer,Reader> readers;

    private static String strFind;
    private static int indexReader;

    public GUIMenuNewReader menuAddReader;

    public static String getStrFind() {
        return strFind;
    }

    public static int getIndexReader() {
        return indexReader;
    }

    public static void setReaders(HashMap<Integer, Reader> readers) {
        GUIMainMenuAdmin.readers = readers;
    }

    public GUIMainMenuAdmin(){

        super("Библиотека");
        this.setBounds(350, 120, 700, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        PanelWhole = this.getContentPane();
        PanelWhole.setBackground(new Color(222, 238, 252));
        PanelWhole.setLayout(null);

        PanelReader = new JPanel();
        PanelReader.setBorder(BorderFactory.createTitledBorder("Читатели"));
        PanelReader.setBounds(10, 20, 650, 270);
        PanelReader.setLayout(null);
        PanelReader.setBackground(new Color(219, 232, 252));

        PanelProperties = new JPanel();
        PanelProperties.setBackground(Color.WHITE);
        PanelProperties.setBorder(BorderFactory.createTitledBorder("Дополнительно"));
        PanelProperties.setBounds(10, 305, 650, 100);
        PanelProperties.setLayout(null);
        PanelProperties.setBackground(new Color(219, 232, 252));

        fieldFind = new JTextField("");
        fieldFind.setBounds(10, 20, 200,25);
        buttonFind = new JButton("Найти");
        buttonFind.setBounds(235, 20,100,25);

        model = new TableModelReaders(readers);
        table = new JTable(model);
        RowSorter<TableModel> sorter  = new TableRowSorter<TableModel>(model);
        table.setRowSorter(sorter);
        scrollPane = new JScrollPane(table);
        scrollPane.setBounds(5,65,640,145);

        buttonOrder = new JButton("Работа с заказами");
        buttonOrder.setBounds(10,220, 135,25);
        buttonOrder.setEnabled(false);

        buttonRemoveReader = new JButton("Удалить");
        buttonRemoveReader.setBounds(165,220, 100,25);
        buttonRemoveReader.setEnabled(false);

        buttonAddReader = new JButton("Добавить нового читателя");
        buttonAddReader.setBounds(400,220, 180,25);

        buttonExit = new JButton("Выход");
        buttonExit.setBounds(12,430, 100,25);

        buttonListLate = new JButton("Список должников");
        buttonListLate.setBounds(15,30, 190,25);

        buttonGraphic = new JButton("Статистика работы");
        buttonGraphic.setBounds(15,65,190,25);

        buttonReportOrders = new JButton("Отчет о заказах");
        buttonReportOrders.setBounds(220,30,190,25);

        buttonReportReaders = new JButton("Отчет о читателях");
        buttonReportReaders.setBounds(220,65,190,25);

        PanelReader.add(fieldFind);
        PanelReader.add(buttonFind);
        PanelReader.add(scrollPane);
        PanelReader.add(buttonOrder);
        PanelReader.add(buttonAddReader);
        PanelReader.add(buttonRemoveReader);

        PanelProperties.add(buttonListLate);
        PanelProperties.add(buttonGraphic);
        PanelProperties.add(buttonReportOrders);
        PanelProperties.add(buttonReportReaders);

        PanelWhole.add(PanelReader);
        PanelWhole.add(PanelProperties);
        PanelWhole.add(buttonExit);
        initListeners();
    }

    public void updateTable(){
        scrollPane.setVisible(false);
        model = new TableModelReaders(readers);
        table = new JTable(model);
        scrollPane = new JScrollPane(table);
        scrollPane.setBounds(5,65,640,145);
        scrollPane.setVisible(true);
        PanelReader.add(scrollPane);
        table.addMouseListener(new MouseListener() {
            public void mouseClicked(MouseEvent e) {
                buttonOrder.setEnabled(true);
                buttonRemoveReader.setEnabled(true);
            }
            public void mouseEntered(MouseEvent e) {}
            public void mouseExited(MouseEvent e) {}
            public void mousePressed(MouseEvent e) {}
            public void mouseReleased(MouseEvent e) {}
        });
    }

    private void initListeners() {
        table.addMouseListener(new MouseListener() {
            public void mouseClicked(MouseEvent e) {
                buttonOrder.setEnabled(true);
                buttonRemoveReader.setEnabled(true);
            }
            public void mouseEntered(MouseEvent e) {}
            public void mouseExited(MouseEvent e) {}
            public void mousePressed(MouseEvent e) {}
            public void mouseReleased(MouseEvent e) {}
        });
        buttonFind.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                strFind = fieldFind.getText();
                GUIMainMenuAdmin.choise = 2;
                CheckThread thread = new CheckThread();
                thread.start();
            }
        });
        buttonOrder.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int rowIndex = table.convertRowIndexToModel(table.getSelectedRow());
                GUIMenuAllOrders.setIndexReader(model.getIndexReader(rowIndex));
                setVisible(false);
                GUIMainMenuAdmin.choise = 3;
                CheckThread thread = new CheckThread();
                thread.start();
            }
        });
        buttonRemoveReader.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object[] options = {"Да",
                        "Отмена"};
                int check = JOptionPane.showOptionDialog(new Container(),
                        "Вы действительно желаете удалить данного читателя из библиотеки?",
                        "Удаление",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        options,
                        options[0]);
                if (check == 0 ) {
                    int rowIndex = table.convertRowIndexToModel(table.getSelectedRow());
                    GUIMainMenuAdmin.choise = 4;
                    indexReader = model.getKey(rowIndex);
                    CheckThread thread = new CheckThread();
                    thread.start();
                }
            }
        });
        buttonAddReader.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                menuAddReader = new GUIMenuNewReader();
                menuAddReader.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                setVisible(false);
                menuAddReader.setVisible(true);
            }
        });
        buttonListLate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                GUIMainMenuAdmin.choise = 7;
                CheckThread thread = new CheckThread();
                thread.start();
            }
        });
        buttonGraphic.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                GUIMainMenuAdmin.choise = 8;
                CheckThread thread = new CheckThread();
                thread.start();
            }
        });
        buttonReportOrders.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                choise = 9;
                CheckThread thread = new CheckThread();
                thread.start();
            }
        });
        buttonReportReaders.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                choise = 10;
                CheckThread thread = new CheckThread();
                thread.start();
            }
        });
        buttonExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                choise = 0;
                CheckThread thread = new CheckThread();
                thread.start();
            }
        });
    }
}
