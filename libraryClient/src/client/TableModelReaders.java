package client;

import library.Reader;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

/**
 * Created by Босс on 12.11.2016.
 */
public class TableModelReaders implements TableModel{
    private Set<TableModelListener> listeners = new HashSet<TableModelListener>();

    private Map<Integer, Reader> readers;
    private LinkedList<Reader> list;
    private Integer[] keyList;

    public TableModelReaders(Map<Integer,Reader> readers) {
        this.readers = readers;
        list = new LinkedList<>();
        keyList = new Integer[readers.size()];
        int size = 0;
        for (Map.Entry entry: readers.entrySet()) {
            keyList[size] = (Integer)entry.getKey();
            list.add((Reader)entry.getValue());
            size++;
        }
    }

    public int getKey(int index){
        return keyList[index];
    }

    public void addTableModelListener(TableModelListener listener) {
        listeners.add(listener);
    }

    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    public int getColumnCount() {
        return 6;
    }

    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return "Номер карточки";
            case 1:
                return "Фамилия";
            case 2:
                return "Имя";
            case 3:
                return "Отчество";
            case 4:
                return "Год рождения";
            case 5:
                return "Мобильный телефон";
        }
        return "";
    }

    public int getIndexReader(int rowIndex){
        return keyList[rowIndex];
    }

    public int getRowCount() {
        return readers.size();
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        Reader reader = list.get(rowIndex);
        switch (columnIndex) {
            case 0:
                if(keyList[rowIndex]<10)
                    return "0" + Integer.valueOf(keyList[rowIndex]).toString();
                else
                    return Integer.valueOf(keyList[rowIndex]).toString();
            case 1:
                return reader.getSurname();
            case 2:
                return reader.getName();
            case 3:
                return reader.getLastname();
            case 4:
                return reader.getDateOfBirth().toString();
            case 5:
                return reader.getMobileNumber();
        }
        return "";
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public void removeTableModelListener(TableModelListener listener) {
        listeners.remove(listener);
    }

    public void setValueAt(Object value, int rowIndex, int columnIndex) {

    }
}
