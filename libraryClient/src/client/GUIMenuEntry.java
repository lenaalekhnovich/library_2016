package client;

import connection.CheckThread;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Босс on 17.10.2016.
 */
public class GUIMenuEntry extends JFrame{

    private static String user;
    private static String password;
    private static boolean isAdmin;

    public GUIMenuRegistration registration;

    public static int checkEntry=0;
    Container container;

    private JButton entryButton = new JButton("Войти");
    private JButton registerButton = new JButton("Регистрация");
    private JTextField loginField = new JTextField("", 10);
    private JPasswordField passwordField = new JPasswordField("", 10);
    private JLabel loginLabel = new JLabel("Логин:");
    private JLabel passwordLabel = new JLabel("Пароль:");
    private JCheckBox check = new JCheckBox("Вход в качестве администратора", false);

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public GUIMenuEntry() {
        super("Вход");
        this.setBounds(500,200,400,250);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Container container = this.getContentPane();
        container.setLayout(null);

        loginLabel.setBounds(50, 40, 100, 20);
        container.add(loginLabel);
        loginField.setBounds(110, 40, 220, 20);
        container.add(loginField);
        passwordLabel.setBounds(50, 75, 100, 20);
        container.add(passwordLabel);
        passwordField.setBounds(110, 75, 220, 20);
        container.add(passwordField);

        check.setBounds(110, 105, 230, 20);
        container.add(check);
        entryButton.setBounds(110, 140, 170, 25);
        container.add(entryButton);
        registerButton.setBounds(110, 180, 170, 25);
        container.add(registerButton);
        initListeners();

    }

    public void showEntry(boolean entry){
        if(entry) {
            setVisible(false);
            JOptionPane.showMessageDialog(container, "Добро пожаловать в библиотеку! :)",
                    "",
                    JOptionPane.INFORMATION_MESSAGE);
            setVisible(false);

        }
        else
            JOptionPane.showMessageDialog(container,"Введен неверный логин или пароль! Пожалуйста, повторите ввод.",
                    "Ошибка",
                    JOptionPane.ERROR_MESSAGE);

    }


    private void initListeners() {
        entryButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                user = loginField.getText();
                password = new String(passwordField.getPassword());
                if(check.isSelected())
                    isAdmin = true;
                else
                    isAdmin = false;
                checkEntry = 1;
                CheckThread thread = new CheckThread();
                thread.start();
            }
        });
        registerButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                registration = new GUIMenuRegistration();
                registration.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                setVisible(false);
                registration.setVisible(true);

            }
        });
    }
}
