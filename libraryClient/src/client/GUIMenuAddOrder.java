package client;


import connection.CheckThread;
import library.Book;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;

/**
 * Created by Босс on 06.11.2016.
 */
public class GUIMenuAddOrder extends JFrame {

    private Container PanelWhole;
    private JPanel PanelOrder;
    private JPanel PanelFind;

    private JButton buttonFind;
    private JButton buttonAddOrder;
    private JButton buttonBack;
    private JButton buttonDescription;
    private JButton buttonLookBook;

    private JLabel labelFind;
    private JTextField fieldFind;

    private JComboBox boxFind;
    private JComboBox boxKindBook;

    private TableModelBooks model;
    private JTable table;
    private JScrollPane scrollPane;

    private static int indexBook;
    private static String kindBook;
    private static String kindFind;
    private static String strFind;

    private static HashMap<Integer, Book> books;
    private RowSorter<TableModel> sorter;

    public static void setBooks(HashMap<Integer, Book> books) {
        GUIMenuAddOrder.books = books;
    }

    public static int getIndexBook() {
        return indexBook;
    }

    public static String getKindBook() {
        return kindBook;
    }

    public static String getKindFind() {
        return kindFind;
    }

    public static String getStrFind() {
        return strFind;
    }

    public GUIMenuAddOrder() {

        super("Библиотека");
        this.setBounds(350, 130, 700, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        PanelWhole = this.getContentPane();
        PanelWhole.setBackground(new Color(222, 238, 252));
        PanelWhole.setLayout(null);
        PanelOrder = new JPanel();
        PanelOrder.setBorder(BorderFactory.createTitledBorder("Документы"));
        PanelOrder.setBounds(10, 20, 650, 270);
        PanelOrder.setLayout(null);
        PanelOrder.setBackground(new Color(219, 232, 252));
        PanelFind = new JPanel();
        PanelFind.setBackground(Color.WHITE);
        PanelFind.setBorder(BorderFactory.createTitledBorder("Поиск"));
        PanelFind.setBounds(10, 315, 650, 100);
        PanelFind.setLayout(null);
        PanelFind.setBackground(new Color(219, 232, 252));

        model = new TableModelBooks(books);
        table = new JTable(model);
        sorter  = new TableRowSorter<TableModel>(model);
        table.setRowSorter(sorter);
        DefaultTableCellRenderer renderer = new DefaultTableCellRenderer(){
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                String checkKind = table.getValueAt(row,table.getColumnModel().getColumnIndex("Вид")).toString();
                if (checkKind.equals("Книга")) {component.setBackground(new Color(252, 194, 199));}
                else if (checkKind.equals("Газета")) {component.setBackground(new Color(147, 252, 179));}
                else if (checkKind.equals("Журнал")) {component.setBackground(new Color(170, 244, 252));}
                else if (checkKind.equals("Аудиодокумент")) {component.setBackground(new Color(195, 172, 252));}
                else if (checkKind.equals("Картографическое издание")) {component.setBackground(new Color(252, 220, 199));}
                else if (checkKind.equals("Нотное издание")) {component.setBackground(new Color(149, 252, 209));}
                else if (checkKind.equals("Редкое издание")) {component.setBackground(new Color(161, 156, 252));}
                else
                    component.setBackground(Color.white);
                return component;
            }
        };
        for(int i=0; i<table.getColumnCount(); i++) {
            table.getColumnModel().getColumn(i).setCellRenderer(renderer);
        }

        scrollPane = new JScrollPane(table);
        scrollPane.setBounds(5, 20, 640, 195);
        buttonAddOrder = new JButton("Добавить");
        buttonAddOrder.setBounds(10, 225, 100, 25);
        buttonAddOrder.setEnabled(false);
        buttonDescription = new JButton("Краткое описание");
        buttonDescription.setBounds(140,225,150,25);
        buttonDescription.setEnabled(false);
        buttonLookBook = new JButton("Просмотреть книгу");
        buttonLookBook.setBounds(320,225,150,25);
        buttonLookBook.setEnabled(false);

        labelFind = new JLabel("Выберете вид:");
        labelFind.setBounds(10, 20, 150, 25);
        labelFind.setHorizontalAlignment(SwingConstants.LEFT);
        String[] itemsKindBook = {
                "Все",
                "Книга",
                "Газета",
                "Журнал",
                "Аудиодокумент",
                "Картографическое издание",
                "Нотное издание"
        };
        String[] itemsFind = {
                "Поиск по автору",
                "Поиск по названию"
        };
        boxKindBook = new JComboBox(itemsKindBook);
        boxKindBook.setBounds(120, 20, 130, 20);
        boxFind = new JComboBox(itemsFind);
        boxFind.setBounds(10, 60, 160, 25);
        fieldFind = new JTextField("");
        fieldFind.setBounds(185, 60, 200, 25);
        buttonFind = new JButton("Найти");
        buttonFind.setBounds(410, 60, 100, 25);
        buttonBack = new JButton("Назад");
        buttonBack.setBounds(12, 430, 100, 25);

        PanelOrder.add(scrollPane);
        PanelOrder.add(buttonAddOrder);
        PanelOrder.add(buttonDescription);
        PanelOrder.add(buttonLookBook);

        PanelFind.add(labelFind);
        PanelFind.add(boxKindBook);
        PanelFind.add(boxFind);
        PanelFind.add(fieldFind);
        PanelFind.add(buttonFind);

        PanelWhole.add(PanelOrder);
        PanelWhole.add(PanelFind);
        PanelWhole.add(buttonBack);
        initListeners();
    }

    public void updateTable() {
        scrollPane.setVisible(false);
        model = new TableModelBooks(books);
        table = new JTable(model);
        DefaultTableCellRenderer renderer = new DefaultTableCellRenderer(){
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                String checkKind = table.getValueAt(row,table.getColumnModel().getColumnIndex("Вид")).toString();
                if (checkKind.equals("Книга")) {component.setBackground(new Color(252, 194, 199));}
                else if (checkKind.equals("Газета")) {component.setBackground(new Color(147, 252, 179));}
                else if (checkKind.equals("Журнал")) {component.setBackground(new Color(170, 244, 252));}
                else if (checkKind.equals("Аудиодокумент")) {component.setBackground(new Color(195, 172, 252));}
                else if (checkKind.equals("Картографическое издание")) {component.setBackground(new Color(252, 220, 199));}
                else if (checkKind.equals("Нотное издание")) {component.setBackground(new Color(149, 252, 209));}
                else if (checkKind.equals("Редкое издание")) {component.setBackground(new Color(161, 156, 252));}
                else
                    component.setBackground(Color.white);
                return component;
            }
        };
        for(int i=0; i<table.getColumnCount(); i++) {
            table.getColumnModel().getColumn(i).setCellRenderer(renderer);
        }
        scrollPane = new JScrollPane(table);
        scrollPane.setBounds(5, 20, 640, 195);
        scrollPane.setVisible(true);
        PanelOrder.add(scrollPane);
        table.addMouseListener(new MouseListener() {
            public void mouseClicked(MouseEvent e) {
                if (model.checkAmount(table.getSelectedRow()))
                    buttonAddOrder.setEnabled(true);
                else
                    buttonAddOrder.setEnabled(false);
                buttonDescription.setEnabled(true);
                buttonLookBook.setEnabled(true);
            }

            public void mouseEntered(MouseEvent e) {
            }

            public void mouseExited(MouseEvent e) {
            }

            public void mousePressed(MouseEvent e) {
            }

            public void mouseReleased(MouseEvent e) {
            }
        });
    }

    public static void successAddOrder(){
        JOptionPane.showMessageDialog(new Container(), "Ваш заказ успешно добавлен!",
                "",
                JOptionPane.INFORMATION_MESSAGE);
    }

    public static void showDescription(String description){
        int index = 500;
        String readyStr = "<html>";
        for(int i = 0; i < description.length();i++){
            readyStr += description.charAt(i);
            if(i % 35 == 0 && i  != 0){index = description.indexOf(" ",i);}
            if(index == i){readyStr += "<br>";}
        }
        readyStr += "</html>";
        JFrame des = new JFrame("Краткое описание");
        JLabel label = new JLabel();
        label.setVerticalAlignment(JLabel.CENTER);
        label.setHorizontalAlignment(JLabel.CENTER);
        label.setFont(new Font("Century Gothic", Font.BOLD, 14));
        des.setBounds(500,230,350,250);
        label.setSize(350,250);
        label.setText(readyStr);
        des.add(label);
        des.setVisible(true);
    }

    private void initListeners() {
       table.addMouseListener(new MouseListener() {
           public void mouseClicked(MouseEvent e) {
               if(model.checkAmount(table.getSelectedRow()))
                   buttonAddOrder.setEnabled(true);
               else
                   buttonAddOrder.setEnabled(false);
               buttonDescription.setEnabled(true);
               buttonLookBook.setEnabled(true);
           }
            public void mouseEntered(MouseEvent e) {}
           public void mouseExited(MouseEvent e) {}
           public void mousePressed(MouseEvent e) {}
           public void mouseReleased(MouseEvent e) {}
        });
        buttonAddOrder.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if(table.getValueAt(table.getSelectedRow(),table.getColumnModel().getColumnIndex("Вид")).toString().equals("Картографическое издание"))
                    JOptionPane.showMessageDialog(new Container(), "Картографические издания не выдаются на руки. Обратитесь к бибилотекарю.",
                            "",
                            JOptionPane.INFORMATION_MESSAGE);
                else {
                    GUIMainMenu.choise = 5;
                    int rowIndex = table.convertRowIndexToModel(table.getSelectedRow());
                    indexBook = model.getKey(rowIndex);
                    CheckThread thread = new CheckThread();
                    thread.start();
                }
            }
        });
        buttonFind.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                kindBook = boxKindBook.getSelectedItem().toString();
                if(boxFind.getSelectedItem().toString().equals("Поиск по автору"))
                    kindFind = "Автор";
                else
                    kindFind = "Название";
                strFind = fieldFind.getText();
                GUIMainMenu.choise = 4;
                CheckThread thread = new CheckThread();
                thread.start();
            }
        });
        buttonDescription.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GUIMainMenu.choise = 6;
                int rowIndex = table.convertRowIndexToModel(table.getSelectedRow());
                indexBook = model.getKey(rowIndex);
                CheckThread thread = new CheckThread();
                thread.start();
            }
        });
        buttonLookBook.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GUIMainMenu.choise = 7;
                CheckThread thread = new CheckThread();
                thread.start();
            }
        });
        buttonBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                GUIMainMenu.choise = 1;
                CheckThread thread = new CheckThread();
                thread.start();
            }
        });
    }


}
