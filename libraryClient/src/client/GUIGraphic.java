package client;

import connection.CheckThread;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.Day;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.Map;

/**
 * Created by Босс on 18.11.2016.
 */
public class GUIGraphic extends JFrame{

    private JButton buttonBack;
    private JPanel chartPanel;

    private static Map<Date, Integer> mapOrders;
    private static Map<Date, Integer> mapReturner;

    public static void setMapOrders(Map<Date, Integer> mapOrders) {
        GUIGraphic.mapOrders = mapOrders;
    }

    public static void setMapReturner(Map<Date, Integer> mapReturner) {
        GUIGraphic.mapReturner = mapReturner;
    }

    public GUIGraphic(){
        super("Статистика работы");
        Container container;
        this.setBounds(400, 130, 700, 540);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        container = this.getContentPane();
        container.setBackground(new Color(222, 238, 252));
        chartPanel = new JPanel();
        chartPanel.setBounds(0,0,700,500);
        chartPanel.setBackground(new Color(222, 238, 252));

        TimeSeries seriesOrders = new TimeSeries("Выдача документов");
        TimeSeries seriesReturner = new TimeSeries("Возврат документов");
        for (Map.Entry entry: mapOrders.entrySet()) {
            seriesOrders.add(new Day((Date)entry.getKey()),(Integer)entry.getValue());
        }
        for (Map.Entry entry: mapReturner.entrySet()) {
            seriesReturner.add(new Day((Date)entry.getKey()),(Integer)entry.getValue());
        }
        TimeSeriesCollection dataset = new TimeSeriesCollection();
        dataset.addSeries(seriesOrders);
        dataset.addSeries(seriesReturner);
        JFreeChart chart = ChartFactory.createTimeSeriesChart("График работы библиотеки","","Количество документов",
                        dataset,true,true,true);
        chart.setBackgroundPaint(new Color(222, 238, 252));
        XYPlot plot = (XYPlot) chart.getPlot();
        plot.setBackgroundPaint(new Color(252, 243, 227));
        plot.getRenderer().setSeriesPaint(0, Color.CYAN);
        NumberAxis axisR = (NumberAxis)plot.getRangeAxis();
        axisR.setStandardTickUnits(NumberAxis.createStandardTickUnits());
        axisR.setTickUnit(new NumberTickUnit(1));
        chartPanel.add(new ChartPanel(chart));
        buttonBack = new JButton("Назад");
        buttonBack.setBounds(20,450, 120,25);
        container.add(buttonBack);
        container.add(chartPanel);
        initListeners();
    }

    private void initListeners() {
        buttonBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                GUIMainMenuAdmin.choise = 1;
                CheckThread thread = new CheckThread();
                thread.start();
            }
        });
    }
}
