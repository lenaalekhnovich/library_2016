package client;

import library.Order;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

/**
 * Created by Босс on 13.11.2016.
 */
public class TableModelAllOrders implements TableModel {
    private Set<TableModelListener> listeners = new HashSet<TableModelListener>();

    private Map<Integer,Order> orders;
    private LinkedList<Order> list;
    private Integer[] keyList;

    public TableModelAllOrders(Map<Integer,Order> orders) {
        this.orders = orders;
        list = new LinkedList<>();
        keyList = new Integer[orders.size()];
        int size = 0;
        for (Map.Entry entry: orders.entrySet()) {
            keyList[size] = (Integer)entry.getKey();
            list.add((Order)entry.getValue());
            size++;
        }
    }

    public int getKey(int index){
        return keyList[index];
    }

    public void addTableModelListener(TableModelListener listener) {
        listeners.add(listener);
    }

    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    public int getColumnCount() {
        return 6;
    }

    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return "Вид";
            case 1:
                return "Автор";
            case 2:
                return "Название";
            case 3:
                return "Дата выдачи";
            case 4:
                return "Дата возврата";
            case 5:
                return "Срок выдачи";
        }
        return "";
    }

    public int getRowCount() {
        return orders.size();
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        Order order = list.get(rowIndex);
        SimpleDateFormat format1 = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        switch (columnIndex) {
            case 0:
                return order.getBook().getKind().getNameKind();
            case 1:
                return order.getBook().getAuthor();
            case 2:
                return order.getBook().getName();
            case 3:
                return format1.format(order.getDateTaken());
            case 4:
                if(order.getDateGiven() != null)
                    return format1.format(order.getDateGiven());
                else
                    return "на руках";
            case 5:
                return order.getBook().getKind().getTimeTaken()+" дней";
        }
        return "";
    }

    public boolean checkDate(int rowIndex){
        Order order = list.get(rowIndex);
        if(order.getDateGiven() == null)
            return true;
        return false;
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public void removeTableModelListener(TableModelListener listener) {
        listeners.remove(listener);
    }

    public void setValueAt(Object value, int rowIndex, int columnIndex) {}
}
