package client;

import library.LibraryCard;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Босс on 16.11.2016.
 */
public class TableModelLateReaders implements TableModel {
    private Set<TableModelListener> listeners = new HashSet<TableModelListener>();

    private List<LibraryCard> cards;

    public TableModelLateReaders(List<LibraryCard> cards) {
        this.cards = cards;
    }

    public void addTableModelListener(TableModelListener listener) {
        listeners.add(listener);
    }

    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    public int getColumnCount() {
        return 6;
    }

    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return "Фамилия";
            case 1:
                return "Имя";
            case 2:
                return "Отчество";
            case 3:
                return "Название";
            case 4:
                return "Автор";
            case 5:
                return "Дата выдачи";
        }
        return "";
    }

    public int getRowCount() {
        return cards.size();
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        LibraryCard card = cards.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return card.getReader().getSurname();
            case 1:
                return card.getReader().getName();
            case 2:
                return card.getReader().getLastname();
            case 3:
                return card.getOrder().get(0).getBook().getName();
            case 4:
                return card.getOrder().get(0).getBook().getAuthor();
            case 5:
                SimpleDateFormat format1 = new SimpleDateFormat("dd.MM.yyyy HH:mm");
                return format1.format(card.getOrder().get(0).getDateTaken());
        }
        return "";
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public void removeTableModelListener(TableModelListener listener) {
        listeners.remove(listener);
    }

    public void setValueAt(Object value, int rowIndex, int columnIndex) {}
}
