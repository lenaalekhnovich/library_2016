package client;

import connection.CheckThread;
import library.LibraryCard;

import javax.swing.*;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

/**
 * Created by Босс on 16.11.2016.
 */
public class GUIMenuLateReaders extends JFrame {
    private Container PanelWhole;
    private JPanel PanelReaders;

    private JButton buttonBack;

    private TableModelLateReaders model;
    private JTable table;
    private JScrollPane scrollPane;

    private static LinkedList<LibraryCard> readers;

    public static void setReaders(LinkedList<LibraryCard> readers) {
        GUIMenuLateReaders.readers = readers;
    }

    public GUIMenuLateReaders() {
        super("Список должников");
        this.setBounds(350, 120, 700, 330);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        PanelWhole = this.getContentPane();
        PanelWhole.setBackground(new Color(222, 238, 252));
        PanelWhole.setLayout(null);
        PanelReaders = new JPanel();
        PanelReaders.setBounds(10, 20, 650, 210);
        PanelReaders.setLayout(null);
        PanelReaders.setBackground(new Color(219, 232, 252));

        model = new TableModelLateReaders(readers);
        table = new JTable(model);
        RowSorter<TableModel> sorter  = new TableRowSorter<TableModel>(model);
        table.setRowSorter(sorter);
        scrollPane = new JScrollPane(table);
        scrollPane.setBounds(5, 5, 640, 195);

        buttonBack = new JButton("Назад");
        buttonBack.setBounds(12, 260, 100, 25);

        PanelReaders.add(scrollPane);

        PanelWhole.add(PanelReaders);
        PanelWhole.add(buttonBack);
        initListeners();
    }

    private void initListeners() {
        buttonBack.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            setVisible(false);
            GUIMainMenuAdmin.choise = 1;
            CheckThread thread = new CheckThread();
            thread.start();
        }
    });
    }
}
