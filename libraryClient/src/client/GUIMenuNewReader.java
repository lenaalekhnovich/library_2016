package client;

import connection.CheckThread;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import library.Reader;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.GregorianCalendar;


import static java.sql.Date.valueOf;

/**
 * Created by Босс on 13.11.2016.
 */
public class GUIMenuNewReader extends JFrame {

    private static Reader reader;

    Container container;

    private JLabel surnameLabel = new JLabel("Фамилия:");
    private JLabel nameLabel = new JLabel("Имя:");
    private JLabel lastnameLabel = new JLabel("Отчество:");
    private JLabel dateLabel = new JLabel("Дата рождения:");
    private JLabel phoneLabel = new JLabel("Телефон:");

    private JTextField surnameField = new JTextField("", 15);
    private JTextField nameField = new JTextField("", 15);
    private JTextField lastnameField = new JTextField("", 15);
    private JTextField dateField = new JTextField("ГГГГ-ММ-ДД", 15);
    private JTextField phoneField = new JTextField("+375", 15);

    private JButton addButton = new JButton("Добавить");
    private JButton backButton = new JButton("Назад");


    public static Reader getReader() {
        return reader;
    }

    public GUIMenuNewReader() {
        super("Добавление читателя");
        this.setBounds(475, 200, 450, 330);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        container = this.getContentPane();
        container.setLayout(null);

        surnameLabel.setBounds(20, 50, 120, 20);
        container.add(surnameLabel);
        surnameField.setBounds(150, 50, 200, 20);
        container.add(surnameField);
        nameLabel.setBounds(20, 80, 120, 20);
        container.add(nameLabel);
        nameField.setBounds(150, 80, 200, 20);
        container.add(nameField);
        lastnameLabel.setBounds(20, 110, 120, 20);
        container.add(lastnameLabel);
        lastnameField.setBounds(150, 110, 200, 20);
        container.add(lastnameField);
        dateLabel.setBounds(20, 140, 120, 20);
        container.add(dateLabel);
        dateField.setBounds(150, 140, 200, 20);
        container.add(dateField);
        phoneLabel.setBounds(20, 170, 120, 20);
        container.add(phoneLabel);
        phoneField.setBounds(150, 170, 200, 20);
        container.add(phoneField);

        addButton.setBounds(20, 200, 160, 25);
        container.add(addButton);
        backButton.setBounds(20, 250, 90, 25);
        container.add(backButton);
        initListeners();
    }


    public void showSucceedAdd(){
        JOptionPane.showMessageDialog(container, "Читатель успешно добавлен!", "",
                JOptionPane.INFORMATION_MESSAGE);
        setVisible(false);
    }

    public void showErrorAdd(){
        JOptionPane.showMessageDialog(container, "Неверно введена дата рождения! Формат ввода: гггг-мм-дд. Пожалуйста, повторите ввод. ", "Ошибка!",
                JOptionPane.ERROR_MESSAGE);
    }

    private void initListeners() {
        backButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                GUIMainMenuAdmin app = new GUIMainMenuAdmin();
                app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                setVisible(false);
                app.setVisible(true);
            }
        });
        addButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Calendar cal = new GregorianCalendar();
                cal.setLenient (false);
                cal.clear ();
                try {
                    int y = Integer.parseInt (valueOf(dateField.getText()).toString().substring (0, 4));
                    int m = Integer.parseInt (valueOf(dateField.getText()).toString().substring (5, 7));
                    int d = Integer.parseInt (valueOf(dateField.getText()).toString().substring (8, 10));
                    cal.set (y, m - 1, d);
                    java.util.Date dt = cal.getTime ();
                    reader = new Reader(surnameField.getText(),nameField.getText(), lastnameField.getText(), valueOf(dateField.getText()), phoneField.getText());
                    GUIMainMenuAdmin.choise = 5;
                    CheckThread thread = new CheckThread();
                    thread.start();
                }
                catch (NumberFormatException nfe) {showErrorAdd();}
                catch (IllegalArgumentException iae) {showErrorAdd();}
            }
        });
    }

}
