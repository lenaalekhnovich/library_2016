package client;

import connection.CheckThread;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Босс on 17.10.2016.
 */
public class GUIMenuRegistration extends JFrame {


    Container container;

    private String id;
    private String phone;
    private String password;
    private String login;

    private JLabel idLabel = new JLabel("№ билета:");
    private JLabel phoneLabel = new JLabel("Телефон:");
    private JLabel loginLabel = new JLabel("Логин:");
    private JLabel passwordLabel = new JLabel("Пароль:");
    private JLabel rememberLabel = new JLabel("Регистрация возможна только при наличии читательского билета!");

    private JTextField idField = new JTextField("", 8);
    private JTextField phoneField = new JTextField("+375", 11);
    private JTextField loginField = new JTextField("", 15);
    private JTextField passwordField = new JTextField("", 15);

    private JButton registerButton = new JButton("Зарегистрироваться");
    private JButton entryButton = new JButton("Назад");


    public String getId() {
        return id;
    }

    public String getPhone() {
        return phone;
    }

    public String getPassword() {
        return password;
    }

    public String getLogin() {
        return login;
    }

    public GUIMenuRegistration(){
        super("Регистрация");
        this.setBounds(500,200,450,300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

       container = this.getContentPane();
        container.setLayout(null);

        rememberLabel.setBounds(20,20,500,20);
        container.add(rememberLabel);
        idLabel.setBounds(20, 50, 100, 20);
        container.add(idLabel);
        idField.setBounds(100, 50, 200, 20);
        container.add(idField);
        phoneLabel.setBounds(20, 80, 100, 20);
        container.add(phoneLabel);
        phoneField.setBounds(100, 80, 200, 20);
        container.add(phoneField);
        loginLabel.setBounds(20, 110, 100, 20);
        container.add(loginLabel);
        loginField.setBounds(100, 110, 200, 20);
        container.add(loginField);
        passwordLabel.setBounds(20, 140, 100, 20);
        container.add(passwordLabel);
        passwordField.setBounds(100, 140, 200, 20);
        container.add(passwordField);

        registerButton.setBounds(20, 170, 160, 25);
        container.add(registerButton);
        entryButton.setBounds(20, 220, 90, 25);
        container.add(entryButton);
        initListeners();
    }

    public void showRegistrationMessage(int choise) throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
        switch(choise) {
            case 1:
                JOptionPane.showMessageDialog(container, "Неверный номер читательского билета или телефон. Пожалуйста, повторите ввод.",
                    "Ошибка",
                    JOptionPane.ERROR_MESSAGE);
                break;
            case 2:
                JOptionPane.showMessageDialog(container, "Данный пользователь уже зарегистрирован в системе. Пожалуйста, повторите ввод.",
                        "Ошибка",
                        JOptionPane.WARNING_MESSAGE);
                break;
            case 3:
                JOptionPane.showMessageDialog(container, "Данный логин уже занят. Пожалуйста, повторите ввод.",
                        "Ошибка",
                        JOptionPane.WARNING_MESSAGE);
                break;
            case 0:
                JOptionPane.showMessageDialog(container, "Регистрация прошла успешна. Вы можете войти как зарегистрированный пользователь.",
                        "",
                        JOptionPane.INFORMATION_MESSAGE);
                GUIMenuEntry entry = new GUIMenuEntry();
                entry.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                setVisible(false);
                entry.setVisible(true);
                break;
        }
    }

    private void initListeners() {
        entryButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                GUIMenuEntry.checkEntry = 3;
                CheckThread thread = new CheckThread();
                thread.start();
            }
        });
        registerButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                GUIMenuEntry.checkEntry = 2;
                login = loginField.getText();
                password = passwordField.getText();
                phone = phoneField.getText();
                id = idField.getText();
                CheckThread thread = new CheckThread();
                thread.start();
            }
        });
    }
}
