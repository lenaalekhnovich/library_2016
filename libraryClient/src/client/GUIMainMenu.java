package client;

import connection.CheckThread;
import library.LibraryCard;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Босс on 28.10.2016.
 */
public class GUIMainMenu extends JFrame {

    private Container PanelWhole;
    private JPanel PanelInfo;
    private JPanel PanelButton;
    private JPanel PanelOrder;
    private JLabel labelId;
    private JLabel labelReadyId;
    private JLabel labelName;
    private JLabel labelReadyName;
    private JLabel labelDate;
    private JLabel labelReadyDate;
    private JLabel labelPhone;
    private JLabel labelReadyPhone;
    private JLabel labelOrders;
    private JButton buttonChangePassword;
    private JButton buttonAddOrder;
    private JButton buttonExit;

    private static LibraryCard card;
    public GUIMenuNewPassword changePassword;
    GUIMenuEntry menuEntry;

    public static int choise = 0;

    public static void setCard(LibraryCard card) {
        GUIMainMenu.card = card;
    }

    public GUIMainMenu() {
        super("Библиотека");
        this.setBounds(400, 130, 610, 430);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        String fio = card.getReader().getSurname() + " " + card.getReader().getName().substring(0,1) + ". " + card.getReader().getLastname().substring(0,1) + ".";
        PanelWhole = this.getContentPane();
        PanelWhole.setBackground(new Color(219, 232, 252));
        PanelWhole.setLayout(null);

        PanelInfo = new JPanel();
        PanelInfo.setBackground(Color.WHITE);
        PanelInfo.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
        PanelInfo.setBounds(15, 20, 300, 150);
        PanelInfo.setLayout(new GridLayout(4, 2, 0,0));

        PanelButton = new JPanel();
        PanelButton.setBackground(new Color(219, 232, 252));
        PanelButton.setBounds(330, 0, 150, 150);
        PanelButton.setLayout(null);

        PanelOrder = new JPanel();
        PanelOrder.setBackground(new Color(219, 232, 252));
        PanelOrder.setBorder(BorderFactory.createTitledBorder("Документы у вас на руках"));
        PanelOrder.setBounds(10,200,580,150);
        PanelOrder.setLayout(null);

        labelId = new JLabel("№ карточки:");
        labelReadyId = new JLabel(card.getId());
        labelName = new JLabel("ФИО:");
        labelReadyName = new JLabel(fio);
        labelReadyName.setSize(500,30);
        labelDate = new JLabel("Дата рождения:");
        labelReadyDate = new JLabel(card.getReader().getDateOfBirth().toString());
        labelPhone = new JLabel("Телефон:");
        labelReadyPhone = new JLabel(card.getReader().getMobileNumber());
        labelOrders = new JLabel("Документы у вас на руках:");
        labelOrders.setBounds(0,200,580,40);
        labelOrders.setHorizontalAlignment(SwingConstants.CENTER);
        labelOrders.setFont(new Font("Arial", Font.ITALIC,14));
        labelOrders.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));

        buttonChangePassword = new JButton("Смена пароля");
        buttonChangePassword.setBounds(20, 55, 120, 25);
        buttonAddOrder = new JButton("Новый заказ");
        buttonAddOrder.setBounds(20, 105, 120, 25);
        buttonExit = new JButton("Выход");
        buttonExit.setBounds(10, 365, 90, 25);

        TableModel model = new TableModelOrders(card.getOrder());
        JTable table = new JTable(model);
        RowSorter<TableModel> sorter  = new TableRowSorter<TableModel>(model);
        table.setRowSorter(sorter);
        final JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.setBounds(5,20,570,125);

        PanelInfo.add(labelId);
        PanelInfo.add(labelReadyId);
        PanelInfo.add(labelName);
        PanelInfo.add(labelReadyName);
        PanelInfo.add(labelDate);
        PanelInfo.add(labelReadyDate);
        PanelInfo.add(labelPhone);
        PanelInfo.add(labelReadyPhone);
        PanelOrder.add(scrollPane);

        PanelButton.add(buttonChangePassword);
        PanelButton.add(buttonAddOrder);

        PanelWhole.add(PanelInfo);
        PanelWhole.add(PanelButton);
        PanelWhole.add(PanelOrder);
        PanelWhole.add(buttonExit);
        initListeners();
    }

    private void initListeners() {
        buttonChangePassword.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                changePassword = new GUIMenuNewPassword();
                changePassword.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                setVisible(false);
                changePassword.setVisible(true);
            }
        });
        buttonAddOrder.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                choise = 3;
                CheckThread thread = new CheckThread();
                thread.start();
            }
        });
        buttonExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                choise = 0;
                CheckThread thread = new CheckThread();
                thread.start();
            }
        });
    }

    public GUIMenuEntry getMenuEntry() {
        return menuEntry;
    }
}