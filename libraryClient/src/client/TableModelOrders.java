package client;

import library.LibraryCard;
import library.Order;

import javax.swing.*;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Босс on 30.10.2016.
 */
public class TableModelOrders implements TableModel {

    private Set<TableModelListener> listeners = new HashSet<TableModelListener>();

    private List<Order> orders;

    public TableModelOrders(List<Order> orders) {
        this.orders = orders;
    }

    public void addTableModelListener(TableModelListener listener) {
        listeners.add(listener);
    }

    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    public int getColumnCount() {
        return 5;
    }

    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return "Вид";
            case 1:
                return "Автор";
            case 2:
                return "Название";
            case 3:
                return "Дата выдачи";
            case 4:
                return "Срок выдачи";
        }
        return "";
    }

    public int getRowCount() {
        return orders.size();
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        Order order = orders.get(rowIndex);

        switch (columnIndex) {
            case 0:
                return order.getBook().getKind().getNameKind();
            case 1:
                return order.getBook().getAuthor();
            case 2:
                return order.getBook().getName();
            case 3:
                SimpleDateFormat format1 = new SimpleDateFormat("dd.MM.yyyy HH:mm");
                return format1.format(order.getDateTaken());
            case 4:
                return order.getBook().getKind().getTimeTaken()+" дней";
        }
        return "";
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public void removeTableModelListener(TableModelListener listener) {
        listeners.remove(listener);
    }

    public void setValueAt(Object value, int rowIndex, int columnIndex) {}

}

