package client;

import connection.CheckThread;
import library.Order;

import javax.swing.*;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;

/**
 * Created by Босс on 13.11.2016.
 */
public class GUIMenuAllOrders extends JFrame{
    private Container PanelWhole;
    private JPanel PanelOrder;

    private JButton buttonMakeAsGiven;
    private JButton buttonBack;


    private TableModelAllOrders model;
    private JTable table;
    private JScrollPane scrollPane;

    private static int indexOrder;
    private static int indexReader;

    private static HashMap<Integer, Order> orders;

    public static void setOrders(HashMap<Integer,Order> orders) {
        GUIMenuAllOrders.orders = orders;
    }

    public static void setIndexReader(int indexReader) {
        GUIMenuAllOrders.indexReader = indexReader;
    }

    public static int getIndexOrder() {
        return indexOrder;
    }

    public static int getIndexReader() {
        return indexReader;
    }

    public GUIMenuAllOrders(){
        super("Работа с заказами");
        this.setBounds(350, 120, 700, 400);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        PanelWhole = this.getContentPane();
        PanelWhole.setBackground(new Color(222, 238, 252));
        PanelWhole.setLayout(null);
        PanelOrder = new JPanel();
        PanelOrder.setBorder(BorderFactory.createTitledBorder("Заказы"));
        PanelOrder.setBounds(10, 20, 650, 270);
        PanelOrder.setLayout(null);
        PanelOrder.setBackground(new Color(219, 232, 252));

        model = new TableModelAllOrders(orders);
        table = new JTable(model);
        RowSorter<TableModel> sorter  = new TableRowSorter<TableModel>(model);
        table.setRowSorter(sorter);
        scrollPane = new JScrollPane(table);
        scrollPane.setBounds(5,20,640,195);
        buttonMakeAsGiven = new JButton("Отметить возврат книги");
        buttonMakeAsGiven.setBounds(10,225, 190,25);
        buttonMakeAsGiven.setEnabled(false);


        buttonBack = new JButton("Назад");
        buttonBack.setBounds(12,330, 100,25);

        PanelOrder.add(scrollPane);
        PanelOrder.add(buttonMakeAsGiven);

        PanelWhole.add(PanelOrder);
        PanelWhole.add(buttonBack);
        initListeners();
    }

    public void updateTable(){
        scrollPane.setVisible(false);
        model = new TableModelAllOrders(orders);
        table = new JTable(model);
        scrollPane = new JScrollPane(table);
        scrollPane.setBounds(5,20,640,195);
        scrollPane.setVisible(true);
        PanelOrder.add(scrollPane);
        table.addMouseListener(new MouseListener() {
            public void mouseClicked(MouseEvent e) {
                if(model.checkDate(table.getSelectedRow()))
                    buttonMakeAsGiven.setEnabled(true);
                else
                    buttonMakeAsGiven.setEnabled(false);
            }
            public void mouseEntered(MouseEvent e) {}
            public void mouseExited(MouseEvent e) {}
            public void mousePressed(MouseEvent e) {}
            public void mouseReleased(MouseEvent e) {}
        });
    }

    private void initListeners() {
        table.addMouseListener(new MouseListener() {
            public void mouseClicked(MouseEvent e) {
                if(model.checkDate(table.getSelectedRow()))
                    buttonMakeAsGiven.setEnabled(true);
                else
                    buttonMakeAsGiven.setEnabled(false);
            }
            public void mouseEntered(MouseEvent e) {}
            public void mouseExited(MouseEvent e) {}
            public void mousePressed(MouseEvent e) {}
            public void mouseReleased(MouseEvent e) {}
        });
        buttonMakeAsGiven.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GUIMainMenuAdmin.choise = 6;
                int rowIndex = table.convertRowIndexToModel(table.getSelectedRow());
                indexOrder = model.getKey(rowIndex);
                CheckThread thread = new CheckThread();
                thread.start();
            }
        });
        buttonBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                GUIMainMenuAdmin.choise = 1;
                CheckThread thread = new CheckThread();
                thread.start();
            }
        });
    }
}

