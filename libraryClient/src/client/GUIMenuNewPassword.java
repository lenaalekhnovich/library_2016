package client;

import connection.CheckThread;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Босс on 03.11.2016.
 */
public class GUIMenuNewPassword extends JFrame{

    private static String oldPassword;
    private static String newPassword;

    Container container;

    private JLabel oldPasswordLabel = new JLabel("Настоящий пароль:");
    private JLabel newPasswordLabel = new JLabel("Новый пароль:");

    private JTextField oldField = new JTextField("", 15);
    private JTextField newField = new JTextField("", 15);

    private JButton changeButton = new JButton("Изменить пароль");
    private JButton backButton = new JButton("Назад");

    public static String getOldPassword() {
        return oldPassword;
    }

    public static String getNewPassword() {
        return newPassword;
    }

    public GUIMenuNewPassword() {
        super("Изменение пароля");
        this.setBounds(400, 200, 450, 250);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        container = this.getContentPane();
        container.setLayout(null);

        oldPasswordLabel.setBounds(20, 50, 120, 20);
        container.add(oldPasswordLabel);
        oldField.setBounds(150, 50, 200, 20);
        container.add(oldField);
        newPasswordLabel.setBounds(20, 80, 120, 20);
        container.add(newPasswordLabel);
        newField.setBounds(150, 80, 200, 20);
        container.add(newField);

        changeButton.setBounds(20, 110, 160, 25);
        container.add(changeButton);
        backButton.setBounds(20, 160, 90, 25);
        container.add(backButton);
        initListeners();
    }

    public void showChangePassword(boolean check){
        if(check){
            JOptionPane.showMessageDialog(container, "Пароль успешно изменен.", "",
                        JOptionPane.INFORMATION_MESSAGE);
            GUIMainMenu app = new GUIMainMenu();
            app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            app.setVisible(true);
            setVisible(false);
        }
        else {
            JOptionPane.showMessageDialog(container, "Введен неверный старый пароль! Пожалуйста, повторите ввод.",
                    "Ошибка",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    private void initListeners() {
        backButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                GUIMainMenu app = new GUIMainMenu();
                app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                setVisible(false);
                app.setVisible(true);
            }
        });
        changeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                oldPassword = oldField.getText();
                newPassword = newField.getText();
                GUIMainMenu.choise = 2;
                CheckThread thread = new CheckThread();
                thread.start();
            }
        });
    }
}
