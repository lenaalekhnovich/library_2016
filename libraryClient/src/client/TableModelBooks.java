package client;

import library.Book;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.*;

/**
 * Created by Босс on 06.11.2016.
 */
public class TableModelBooks implements TableModel{
    private Set<TableModelListener> listeners = new HashSet<TableModelListener>();

    private Map<Integer,Book> books;
    private LinkedList<Book> list;
    private Integer[] keyList;

    public TableModelBooks(Map<Integer,Book> books) {
        this.books = books;
        list = new LinkedList<>();
        keyList = new Integer[books.size()];
        int size = 0;
        for (Map.Entry entry: books.entrySet()) {
            keyList[size] = (Integer)entry.getKey();
            list.add((Book)entry.getValue());
            size++;
        }
    }


    public int getKey(int rowIndex){
        return keyList[rowIndex];
    }

    public void addTableModelListener(TableModelListener listener) {
        listeners.add(listener);
    }

    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    public int getColumnCount() {
        return 6;
    }

    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return "Вид";
            case 1:
                return "Автор";
            case 2:
                return "Название";
            case 3:
                return "Издательство";
            case 4:
                return "Год издания";
            case 5:
                return "Количество";
        }
        return "";
    }

    public int getRowCount() {
        return books.size();
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        Book book = list.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return book.getKind().getNameKind();
            case 1:
                return book.getAuthor();
            case 2:
                return book.getName();
            case 3:
                return book.getPublisher().getName();
            case 4:
                return Integer.valueOf(book.getYearPublish()).toString();
            case 5:
                return Integer.valueOf((book.getAmountInLibrary()-book.getAmountTaken())).toString();
        }
        return "";
    }

    public boolean checkAmount(int rowIndex){
        Book book = list.get(rowIndex);
        if((book.getAmountInLibrary()-book.getAmountTaken())!= 0)
            return true;
        return false;
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public void removeTableModelListener(TableModelListener listener) {
        listeners.remove(listener);
    }

    public void setValueAt(Object value, int rowIndex, int columnIndex) {

    }
}
