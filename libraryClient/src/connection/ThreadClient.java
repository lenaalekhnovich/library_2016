package connection;

import client.*;
import library.Book;
import library.LibraryCard;
import library.Order;
import library.Reader;

import javax.swing.*;
import java.io.*;
import java.net.Socket;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;

//import java.io.Reader;

/**
 * Created by Босс on 18.10.2016.
 */
public class ThreadClient extends Thread implements Serializable, ProgramProperties{
    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;
    private ObjectInputStream inputStream;
    private ObjectOutputStream outputStream;

    public ThreadClient() {
        try {
            socket = new Socket("127.0.0.1", 2525);
        }
        catch (IOException e) {
            System.err.println("Socket failed");
            // Если создание сокета провалилось,
            // ничего ненужно чистить.
        }
        try {
            in = new BufferedReader(new InputStreamReader(socket
                    .getInputStream()));
            out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(
                    socket.getOutputStream())), true);
            outputStream = new ObjectOutputStream(socket.getOutputStream());
            inputStream   = new ObjectInputStream(socket.getInputStream());
            start();
        }
        catch (IOException e) {
            // Сокет должен быть закрыт при любой
            // ошибке, кроме ошибки конструктора сокета:
            try {
                socket.close();
            }
            catch (IOException e2) {
                System.err.println("Socket not closed");
            }
        }
    }

    public void run() {
        try {
           openEntryMenu();
        } finally {
            try {
                socket.close();
            }
            catch (IOException e) {
                System.err.println("Socket not closed");
            }
        }
    }

    public void openEntryMenu(){
        try {
            boolean entry = false;
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            GUIMenuEntry app = new GUIMenuEntry();
            app.setVisible(true);
            CheckThread.setObj(app);
            while (true) {
                synchronized (app) {
                    CheckThread.setObj(app);
                    app.wait();
                    out.println(app.checkEntry);
                    switch (app.checkEntry) {
                        case 1:
                            entry = makeEntry(app);
                            if(entry) {
                                app.setVisible(false);
                                if(!app.isAdmin())
                                    app = openUserMenu();
                                else
                                    app = openAdminMenu();
                            }
                            break;
                        case 2:
                            makeRegistration(app);
                            break;
                        case 3:
                            app.setVisible(true);
                            break;
                    }
                }
            }
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
    }

    public boolean makeEntry(GUIMenuEntry app) throws IOException {
        boolean entry;
        out.println(app.getUser());
        out.println(app.getPassword());
        out.println(app.isAdmin());
        entry = Boolean.parseBoolean(in.readLine());
        app.showEntry(entry);
        return entry;
    }

    public void makeRegistration(GUIMenuEntry app) throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException, IOException {
        out.println(app.registration.getId());
        out.println(app.registration.getPhone());
        out.println(app.registration.getLogin());
        out.println(app.registration.getPassword());
        int registration = Integer.parseInt(in.readLine());
        app.registration.showRegistrationMessage(registration);
    }

    public GUIMenuEntry openAdminMenu() throws IOException, ClassNotFoundException, InterruptedException {
        GUIMenuEntry menuEntry = null;
        GUIMenuAllOrders menuOrders = null;
        GUIMenuLateReaders menuLater = null;
        GUIGraphic graphic = null;
        LinkedHashMap<Integer, Reader> map;
        LinkedHashMap<Integer, Order> mapOrder;
        LinkedHashMap<Date, Integer> mapCount;
        LinkedList<LibraryCard> cards;
        map = (LinkedHashMap<Integer,Reader>) inputStream.readObject();
        GUIMainMenuAdmin.setReaders(map);
        GUIMainMenuAdmin mainMenuApp = new GUIMainMenuAdmin();
        mainMenuApp.setVisible(true);
        int choise = 1;
        while (choise != 0) {
            synchronized (mainMenuApp) {
                CheckThread.setObj(mainMenuApp);
                mainMenuApp.wait();
                out.println(mainMenuApp.choise);
                choise = mainMenuApp.choise;
                switch (mainMenuApp.choise) {
                    case 1:   //открытие главного меню, когда back
                        map = (LinkedHashMap<Integer,Reader>) inputStream.readObject();
                        GUIMainMenuAdmin.setReaders(map);
                        mainMenuApp = new GUIMainMenuAdmin();
                        mainMenuApp.setVisible(true);
                        break;
                    case 2:   //поиск читателей
                        out.println(GUIMainMenuAdmin.getStrFind());
                        map = (LinkedHashMap<Integer,Reader>) inputStream.readObject();
                        GUIMainMenuAdmin.setReaders(map);
                        mainMenuApp.updateTable();
                        break;
                    case 3:  //работа с заказами читателя
                        out.println(GUIMenuAllOrders.getIndexReader());
                        mapOrder = (LinkedHashMap<Integer,Order>) inputStream.readObject();
                        GUIMenuAllOrders.setOrders(mapOrder);
                        menuOrders = new GUIMenuAllOrders();
                        menuOrders.setVisible(true);
                        break;
                    case 4:  //удаление читателя
                        out.println(GUIMainMenuAdmin.getIndexReader());
                        map = (LinkedHashMap<Integer,Reader>) inputStream.readObject();
                        GUIMainMenuAdmin.setReaders(map);
                        mainMenuApp.updateTable();
                        break;
                    case 5:  //добавление нового читателя
                        outputStream.writeObject(GUIMenuNewReader.getReader());
                        map = (LinkedHashMap<Integer,Reader>) inputStream.readObject();
                        mainMenuApp.menuAddReader.showSucceedAdd();
                        GUIMainMenuAdmin.setReaders(map);
                        mainMenuApp.updateTable();
                        mainMenuApp.setVisible(true);
                        break;
                    case 6:   //отметить возврат книги
                        out.println(GUIMenuAllOrders.getIndexReader());
                        out.println(GUIMenuAllOrders.getIndexOrder());
                        mapOrder = (LinkedHashMap<Integer,Order>) inputStream.readObject();
                        GUIMenuAllOrders.setOrders(mapOrder);
                        menuOrders.updateTable();
                        break;
                    case 7:   //список должников
                        cards = (LinkedList<LibraryCard>) inputStream.readObject();
                        GUIMenuLateReaders.setReaders(cards);
                        menuLater = new GUIMenuLateReaders();
                        menuLater.setVisible(true);
                        break;
                    case 8:  //график
                        mapCount = (LinkedHashMap<Date,Integer>) inputStream.readObject();
                        GUIGraphic.setMapOrders(mapCount);
                        mapCount = (LinkedHashMap<Date,Integer>) inputStream.readObject();
                        GUIGraphic.setMapReturner(mapCount);
                        graphic = new GUIGraphic();
                        graphic.setVisible(true);
                        break;
                    case 0:
                        menuEntry = new GUIMenuEntry();
                        menuEntry.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                        menuEntry.setVisible(true);
                        mainMenuApp.setVisible(false);
                        break;
                    default:
                        break;
                }
            }
        }
        return menuEntry;
    }

    public GUIMenuEntry openUserMenu() throws IOException, ClassNotFoundException, InterruptedException {
        GUIMenuEntry menuEntry = null;
        boolean checkChangePassword = false;
        GUIMenuAddOrder menuOrder = null;
        LinkedHashMap<Integer,Book> map;
        LibraryCard card = (LibraryCard) inputStream.readObject();
        GUIMainMenu.setCard(card);
        GUIMainMenu mainMenuApp = new GUIMainMenu();
        mainMenuApp.setVisible(true);
        int choise = 1;
        while (choise != 0) {
            synchronized (mainMenuApp) {
                CheckThread.setObj(mainMenuApp);
                mainMenuApp.wait();
                out.println(mainMenuApp.choise);
                choise = mainMenuApp.choise;
                switch (mainMenuApp.choise) {
                    case 1:   //открытие главного меню, когда back
                        card = (LibraryCard) inputStream.readObject();
                        GUIMainMenu.setCard(card);
                        mainMenuApp = new GUIMainMenu();
                        mainMenuApp.setVisible(true);
                        break;
                    case 2:  //изменение пароля
                        out.println(GUIMenuNewPassword.getOldPassword());
                        out.println(GUIMenuNewPassword.getNewPassword());
                        checkChangePassword = Boolean.parseBoolean(in.readLine());
                        mainMenuApp.changePassword.showChangePassword(checkChangePassword);
                        break;
                    case 3:  //список книг(новый заказ)
                        map = (LinkedHashMap<Integer,Book>) inputStream.readObject();
                        GUIMenuAddOrder.setBooks(map);
                        menuOrder = new GUIMenuAddOrder();
                        menuOrder.setVisible(true);
                        break;
                    case 4:  //поиск книг
                        out.println(GUIMenuAddOrder.getKindBook());
                        out.println(GUIMenuAddOrder.getKindFind());
                        out.println(GUIMenuAddOrder.getStrFind());
                        map = (LinkedHashMap<Integer,Book>) inputStream.readObject();
                        GUIMenuAddOrder.setBooks(map);
                        menuOrder.updateTable();
                        break;
                    case 5:  //добавление книги
                        out.println(GUIMenuAddOrder.getIndexBook());
                        GUIMenuAddOrder.successAddOrder();
                        map = (LinkedHashMap<Integer,Book>) inputStream.readObject();
                        GUIMenuAddOrder.setBooks(map);
                        menuOrder.updateTable();
                        break;
                    case 6: //краткое описание
                        out.println(GUIMenuAddOrder.getIndexBook());
                        GUIMenuAddOrder.showDescription(in.readLine());
                        break;
                    case 0:
                        menuEntry = new GUIMenuEntry();
                        menuEntry.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                        menuEntry.setVisible(true);
                        mainMenuApp.setVisible(false);
                        break;
                }
            }
        }
        return menuEntry;
    }

}
