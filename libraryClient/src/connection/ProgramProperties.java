package connection;

import client.GUIMenuEntry;

import javax.swing.*;
import java.io.IOException;

/**
 * Created by Босс on 12.11.2016.
 */
public interface ProgramProperties {
    void openEntryMenu();
    boolean makeEntry(GUIMenuEntry app) throws IOException;
    void makeRegistration(GUIMenuEntry app) throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException, IOException;
    GUIMenuEntry openAdminMenu() throws IOException, ClassNotFoundException, InterruptedException;
    GUIMenuEntry openUserMenu() throws IOException, ClassNotFoundException, InterruptedException;
}
