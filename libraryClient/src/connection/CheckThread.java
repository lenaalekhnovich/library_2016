package connection;

/**
 * Created by Босс on 18.10.2016.
 */
public class CheckThread extends Thread {

    static Object obj;


    public static void setObj(Object obj) {
        CheckThread.obj = obj;
    }


    public void run(){
        synchronized (obj){
            obj.notify();
        }
    }
}
