package library;

import connection.ThreadServer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;

/**
 * Created by Босс on 30.09.2016.
 */
public class LibraryWork {
    public static void main(String args[]) throws SQLException, IOException {
        ServerSocket s = new ServerSocket(2525);
        int count = 0;
        System.out.println("Сервер запущен..");
        try {
            while (true) {
                Socket socket = s.accept();
                try {
                    count++;
                    new ThreadServer(socket, count);
                } catch (IOException e) {
                    socket.close();
                }
            }
        } finally {
            s.close();
        }
    }
}

