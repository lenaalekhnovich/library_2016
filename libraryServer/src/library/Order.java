package library;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Босс on 30.09.2016.
 */
public class Order implements Serializable {
    private Book book;
    private Date dateTaken;
    private Date dateGiven;

    public Order(Book book, Date dateTaken, Date dateGiven) {

        this.book = book;
        this.dateTaken = dateTaken;
        this.dateGiven = dateGiven;

    }

    public Book getBook() {
        return book;
    }

    public Date getDateTaken() {
        return dateTaken;
    }

    public Date getDateGiven() {
        return dateGiven;
    }

    @Override
    public String toString() {
        return "Документ(" + book +
                "), дата выдачи:" + dateTaken +
                ", дата возврата:" + dateGiven;
    }
}
