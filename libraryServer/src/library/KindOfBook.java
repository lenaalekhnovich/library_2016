package library;

import java.io.Serializable;

/**
 * Created by Босс on 16.10.2016.
 */
public class KindOfBook implements Serializable {

    private String nameKind;
    private String nameDepartment;
    private int timeTaken;

    public KindOfBook(String nameKind, String nameDepartment, int timeTaken) {
        this.nameKind = nameKind;
        this.nameDepartment = nameDepartment;
        this.timeTaken = timeTaken;
    }

    public String getNameKind() {
        return nameKind;
    }

    public String getNameDepartment() {
        return nameDepartment;
    }

    public int getTimeTaken() {
        return timeTaken;
    }

    @Override
    public String toString() {
        return "KindOfBook{" +
                "nameKind='" + nameKind + '\'' +
                ", nameDepartment='" + nameDepartment + '\'' +
                ", timeTaken=" + timeTaken +
                '}';
    }
}
