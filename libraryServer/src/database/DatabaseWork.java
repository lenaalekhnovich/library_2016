package database;

import library.*;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Босс on 04.10.2016.
 */
public class DatabaseWork extends DatabaseConnection implements PropertiesDatabaseWork{

    public boolean checkEntry(String login, String password, boolean isAdmin){
        boolean check = false;
        String checkUser = "Нет";
        if (isAdmin)
            checkUser = "Да";
        String selectSQL = "SELECT * FROM `данные пользователя` WHERE `Администратор` = '" + checkUser + "';";
        try {
            statement = dbConnection.createStatement();
            rs = statement.executeQuery(selectSQL);

                while(rs.next()){
                String loginDB= rs.getString("Логин");
                String passwordDB = rs.getString("Пароль");
                    if(login.equals(loginDB) && password.equals(passwordDB)) {
                        check = true;
                        return check;
                    }
                }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return check;
    }

    public int checkRegistration(String id, String phone,String login, String password){
        boolean check;
        String selectSQL = "SELECT * FROM `читатель` WHERE `idЧитатель` = '" + id + "' AND `Мобильный телефон` = '" + phone +"';";
        try {
            statement = dbConnection.createStatement();
            rs = statement.executeQuery(selectSQL);

            while(rs.next()){
                String idDB= rs.getString("idЧитатель");
                String phoneDB = rs.getString("Мобильный телефон");
                if(id.equals(idDB) && phone.equals(phoneDB)) {
                    if(checkId(id)) {
                        if(checkLogin(login, password,id))
                            return 0;
                        else
                            return 3;
                    }
                    else
                        return 2;
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return 1;
    }

    public boolean checkId(String id) throws SQLException {
        boolean check = true;
        String selectSQL = "SELECT * FROM `данные пользователя` WHERE `idДанные` = '" + id + "';";
        try {
            statement = dbConnection.createStatement();
            rs = statement.executeQuery(selectSQL);
            while(rs.next()) {
                String idDB = rs.getString("idДанные");
                if (id.equals(idDB)) {
                    check = false;
                    return check;
                }

            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return check;
    }

    public boolean checkLogin(String login, String password,String id) throws SQLException {
        boolean check = true;
        String selectSQL = "SELECT * FROM `данные пользователя` WHERE `Логин` = '" + login + "';";
        try {
            statement = dbConnection.createStatement();
            rs = statement.executeQuery(selectSQL);
            while(rs.next()) {
                String loginDB = rs.getString("Логин");
                String passwordDB = rs.getString("Пароль");
                if (login.equals(loginDB)) {
                    check = false;
                    return check;
                }

            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        if(check) {
            String insertSQL = "INSERT INTO `данные пользователя` (`idДанные`, `Логин`, `Пароль`, `Администратор`) " +
                    "VALUES('" + id + "', '" + login + "', '" + password
                    + "', 'Нет');";
            statement.executeUpdate(insertSQL);
        }
        return check;
    }

    public boolean changePassword(String login, String oldPassword, String newPassword){
        boolean check = false;
        String selectSQL = "SELECT * FROM `данные пользователя` WHERE `Логин` = '" + login + "';";
        try {
            statement = dbConnection.createStatement();
            rs = statement.executeQuery(selectSQL);
            while(rs.next()) {
                String passwordDB = rs.getString("Пароль");
                if (oldPassword.equals(passwordDB)) {
                    String insertSQL = "UPDATE `database`.`данные пользователя` SET `Пароль` = '" + newPassword +
                            "' WHERE `Логин` = '" + login + "';";
                    statement = dbConnection.createStatement();
                    statement.executeUpdate(insertSQL);
                    check = true;
                    return check;
                }

            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return check;
    }

    public int findIdReader(String login) throws SQLException {
        int id = 0;
        String selectSQL = "SELECT * FROM `данные пользователя` WHERE `Логин` = '" + login + "';";
        try {
            statement = dbConnection.createStatement();
            rs = statement.executeQuery(selectSQL);
            while(rs.next()) {
                id = Integer.parseInt(rs.getString("IdДанные"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return id;
    }

    public Book getBook(int idBook){
        Book book = new Book();
        KindOfBook kind = null;
        Publisher publisher = null;
        String selectSQL = "SELECT * FROM `документ` WHERE `idДокумент` = '" + idBook + "';";
        try {
            statement = dbConnection.createStatement();
            rs = statement.executeQuery(selectSQL);

            while(rs.next()){
                int i = rs.getInt("idВидДокумента");
                int id = rs.getInt("idИздательство");
                String name= rs.getString("Название");
                String author = rs.getString("Автор");
                int yearPublish = rs.getInt("Год издания");
                int amountInLibrary = rs.getInt("Количество в билиотеке");
                int amountTaken = rs.getInt("Количество на руках");
                kind = getKind(i);
                publisher = getPublisher(id);
                book = new Book(author, name, yearPublish, amountInLibrary, amountTaken, publisher, kind);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return book;
    }

    public Reader getReader(int idReader){
        Reader reader = new Reader();
        String selectSQL = "SELECT * FROM `читатель` WHERE `idЧитатель` = '" + idReader + "';";
        try {
            statement = dbConnection.createStatement();
            rs = statement.executeQuery(selectSQL);

            while(rs.next()){
                String surname= rs.getString("Фамилия");
                String name= rs.getString("Имя");
                String lastname= rs.getString("Отчество");
                Date dateOfBirth = rs.getDate("Дата рождения");
                String mobileNumber = rs.getString("Мобильный телефон");
                reader = new Reader(surname, name, lastname, dateOfBirth, mobileNumber);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return reader;
    }

    public Order getOrder(int idOrder){
        Book book;
        Order order = null;
        String selectSQL = "SELECT * FROM `заказ` WHERE `idЗаказ` = '" + idOrder + "';";
        try {
            statement = dbConnection.createStatement();
            rs = statement.executeQuery(selectSQL);
            rs.next();
            int idBook = rs.getInt("idДокумент");
            Date dateTaken = null;
            Date dateGiven = null;
            Timestamp timestamp = rs.getTimestamp("Дата взятия");
            dateTaken = new java.util.Date(timestamp.getTime());
            timestamp = rs.getTimestamp("Дата возврата");
            if(timestamp != null)
                dateGiven = new java.util.Date(timestamp.getTime());
            book = getBook(idBook);
            order = new Order(book, dateTaken, dateGiven);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return order;
    }

    public LibraryCard getLibraryCard(int idReader){
        Reader reader;
        Order order;
        LibraryCard card = new LibraryCard();
        int []idOrder = new int[100];
        int size = 0;
        String selectSQL = "SELECT * FROM `Заказ` WHERE `idЧитатель` = '" + idReader + "' AND `Дата возврата` is null;";
        try {
            statement = dbConnection.createStatement();
            rs = statement.executeQuery(selectSQL);

            while(rs.next()){
                idOrder[size] = rs.getInt("idЗаказ");
                size++;
            }
            for(int i = 0; i < size; i++) {
                order = getOrder(idOrder[i]);
                card.addOrder(order);
            }
            reader = getReader(idReader);
            card.setReader(reader);
            card.setId(Integer.toString(idReader));
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return card;
    }

    public KindOfBook getKind(int idKind){
        KindOfBook kind = null;
        String selectSQL = "SELECT * FROM `вид документа` WHERE `idВидДокумента` = '" + idKind + "';";
        try {
            statement = dbConnection.createStatement();
            rs = statement.executeQuery(selectSQL);
            while(rs.next()){
                String name= rs.getString("Название");
                int time = rs.getInt("Срок выдачи");
                int idDepartment = rs.getInt("idОтдел");
                String nameDepartment = getDepartment(idDepartment);
                kind = new KindOfBook(name,nameDepartment, time);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return kind;
    }

    public int getIdKindBook(String kindBook){
        int id = 0;
        String selectSQL = "SELECT * FROM `вид документа` WHERE `Название` = '" + kindBook + "';";
        try {
            statement = dbConnection.createStatement();
            rs = statement.executeQuery(selectSQL);

            while(rs.next()) {
                id = rs.getInt("idВидДокумента");
                return id;
            }
        }catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return id;
    }

    public Publisher getPublisher(int idPublisher){
        Publisher publisher = null;
        String selectSQL = "SELECT * FROM `издательство` WHERE `idИздательство` = '" + idPublisher + "';";
        try {
            statement = dbConnection.createStatement();
            rs = statement.executeQuery(selectSQL);

            while(rs.next()){
                String name= rs.getString("Название");
                String phone = rs.getString("Телефон");
                publisher = new Publisher(name,phone);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return publisher;
    }

    public String getDepartment(int idDepartment){
        String name = "";
        String selectSQL = "SELECT * FROM `отдел` WHERE `idОтдел` = '" + idDepartment + "';";
        try {
            statement = dbConnection.createStatement();
            rs = statement.executeQuery(selectSQL);

            while(rs.next()){
                name= rs.getString("Название");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return name;
    }

    public String getBookDescription(int idBook){
        String description = null;
        String selectSQL = "SELECT * FROM `документ` WHERE `idДокумент` = '" + idBook + "';";
        try {
            statement = dbConnection.createStatement();
            rs = statement.executeQuery(selectSQL);

            while(rs.next()){
                description = rs.getString("Описание");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return description;
    }


    public Map<Integer,Book> getAllBooks(){
        Map<Integer,Book> map = new LinkedHashMap<>();
        Book book = new Book();
        int []idBook = new int[1000];
        int size = 0;
        String selectSQL = "SELECT * FROM `документ` order by `Автор`";
        try {
            statement = dbConnection.createStatement();
            rs = statement.executeQuery(selectSQL);
            while(rs.next()){
                idBook[size] = rs.getInt("idДокумент");
                size++;
            }
            for(int i = 0; i < size; i++) {
                book = getBook(idBook[i]);
                map.put(idBook[i],book);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return map;
    }

    public Map<Integer,Book> getAllBooks(String kindFind, String strFind){
        Map<Integer,Book> map = new LinkedHashMap<>();
        Book book = new Book();
        int []idBook = new int[1000];
        int size = 0;
        String selectSQL = "SELECT * FROM `документ` WHERE `"+kindFind + "` like '%"+strFind + "%' order by `Автор`";
        try {
            statement = dbConnection.createStatement();
            rs = statement.executeQuery(selectSQL);
            while(rs.next()){
                idBook[size] = rs.getInt("idДокумент");
                size++;
            }
            for(int i = 0; i < size; i++) {
                book = getBook(idBook[i]);
                map.put(idBook[i],book);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return map;
    }

    public Map<Integer,Book> findBook(String kindBook){
        Map<Integer,Book> map = new LinkedHashMap<>();
        Book book = new Book();
        int []idBook = new int[1000];
        int size = 0;
        int idKind = getIdKindBook(kindBook);
        String selectSQL = "SELECT * FROM `документ` WHERE `idВидДокумента` = '" + idKind + "' order by `Автор`";
        try {
            statement = dbConnection.createStatement();
            rs = statement.executeQuery(selectSQL);
            while(rs.next()){
                idBook[size] = rs.getInt("idДокумент");
                size++;
            }
            for(int i = 0; i < size; i++) {
                book = getBook(idBook[i]);
                map.put(idBook[i],book);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return map;
    }

    public Map<Integer,Book> findBook(String kindBook, String kindFind, String strFind){
        Map<Integer,Book> map = new LinkedHashMap<>();
        Book book = new Book();
        int []idBook = new int[1000];
        int size = 0;
        int idKind = getIdKindBook(kindBook);
        String selectSQL = "SELECT * FROM `документ` WHERE `idВидДокумента` = '" + idKind + "' and `"+kindFind + "` like '%"+strFind + "%' order by `Автор`";
        try {
            statement = dbConnection.createStatement();
            rs = statement.executeQuery(selectSQL);
            while(rs.next()){
                idBook[size] = rs.getInt("idДокумент");
                size++;
            }
            for(int i = 0; i < size; i++) {
                book = getBook(idBook[i]);
                map.put(idBook[i],book);

            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return map;
    }


    public LinkedList<Order> getAllOrders(){
        LinkedList<Order> list = new LinkedList<>();
        Order order;
        int []idOrder = new int[1000];
        int size = 0;
        String selectSQL = "SELECT * FROM `Заказ` order by `Дата возврата`;";
        try {
            statement = dbConnection.createStatement();
            rs = statement.executeQuery(selectSQL);
            while(rs.next()){
                idOrder[size] = rs.getInt("idЗаказ");
                size++;
            }
            for(int i = 0; i < size; i++) {
                order = getOrder(idOrder[i]);
                list.add(order);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    public Map<Integer,Order> getAllOrders(int idReader){
        Map<Integer,Order> map = new LinkedHashMap<>();
        Order order;
        int []idOrder = new int[1000];
        int size = 0;
        String selectSQL = "SELECT * FROM `Заказ` WHERE `idЧитатель` = '" + idReader + "' order by `Дата возврата`;";
        try {
            statement = dbConnection.createStatement();
            rs = statement.executeQuery(selectSQL);
            while(rs.next()){
                idOrder[size] = rs.getInt("idЗаказ");
                size++;
            }
            for(int i = 0; i < size; i++) {
                order = getOrder(idOrder[i]);
                map.put(idOrder[i],order);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return map;
    }

    public Map<Integer,Reader> getAllReaders(){
        Map<Integer,Reader> map = new LinkedHashMap<>();
        Reader reader = new Reader();
        int []idReader = new int[1000];
        int size = 0;
        String selectSQL = "SELECT * FROM `читатель` order by `Фамилия`";
        try {
            statement = dbConnection.createStatement();
            rs = statement.executeQuery(selectSQL);
            while(rs.next()){
                idReader[size] = rs.getInt("idЧитатель");
                size++;
            }
            for(int i = 0; i < size; i++) {
                reader = getReader(idReader[i]);
                map.put(idReader[i],reader);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return map;
    }

    public Map<Integer,Reader> findReaders(String strFind){
        Map<Integer,Reader> map = new LinkedHashMap<>();
        Reader reader = new Reader();
        int []idReader = new int[1000];
        int size = 0;
        String selectSQL = "SELECT * FROM `читатель` WHERE `Фамилия` like '%"+strFind + "%' or `Имя` like '%"+strFind + "%' order by `Фамилия`";
        try {
            statement = dbConnection.createStatement();
            rs = statement.executeQuery(selectSQL);
            while(rs.next()){
                idReader[size] = rs.getInt("idЧитатель");
                size++;
            }
            for(int i = 0; i < size; i++) {
                reader = getReader(idReader[i]);
                map.put(idReader[i],reader);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return map;
    }


    public void addOrder(int idReader, int idBook) throws SQLException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = dateFormat.format(new Date()).toString();
        String insertSQL = "INSERT INTO `заказ` (`idДокумент`, `idЧитатель`, `Дата взятия`) " +
                "VALUES(" + idBook + ", " + idReader + ", '" + date + "');";
        statement = dbConnection.createStatement();
        statement.executeUpdate(insertSQL);
        insertSQL = "UPDATE `документ` set `Количество на руках` = `Количество на руках` + 1 where `idДокумент` = " + idBook;
        statement = dbConnection.createStatement();
        statement.executeUpdate(insertSQL);
    }

    public void addReader(Reader reader) throws SQLException {
        String insertSQL = "INSERT INTO `читатель` (`Фамилия`, `Имя`, `Отчество`, `Дата рождения`, `Мобильный телефон`) " +
                "VALUES('" + reader.getSurname() + "', '" + reader.getName() + "', '" + reader.getLastname()
                + "', '" + reader.getDateOfBirth()+ "', '" + reader.getMobileNumber() + "');";
        statement.executeUpdate(insertSQL);
    }

    public void removeReader(int idReader) throws SQLException {
        String deleteSQL = "DELETE from `читатель` where `idЧитатель` = " + idReader;
        statement = dbConnection.createStatement();
        statement.executeUpdate(deleteSQL);
    }

    public void updateOrder(int idOrder) throws SQLException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = dateFormat.format(new Date()).toString();
        String insertSQL = "UPDATE `заказ` set `Дата возврата` = '" + date + "'where `idЗаказ` = " + idOrder;
        statement = dbConnection.createStatement();
        statement.executeUpdate(insertSQL);
        String selectSQL = "SELECT * FROM `заказ` where `idЗаказ` = " + idOrder;
        int idBook = 0;
        statement = dbConnection.createStatement();
        rs = statement.executeQuery(selectSQL);
        while (rs.next()) {
            idBook = rs.getInt("idДокумент");
        }
        insertSQL = "UPDATE `документ` set `Количество на руках` = `Количество на руках` - 1 where `idДокумент` = " + idBook;
        statement = dbConnection.createStatement();
        statement.executeUpdate(insertSQL);
    }


    public List<LibraryCard> listLateReaders(){
        List<LibraryCard> list = new LinkedList<>();
        Reader reader;
        Order order;
        LibraryCard card;
        int []idOrder = new int[100];
        int []idReader = new int[100];
        int size = 0;
        String selectSQL = "SELECT DISTINCT `idЗаказ`, `idЧитатель` FROM `database`.`Заказ`,`database`.`Документ` " +
                "WHERE ((`idВидДокумента`= 1 OR `idВидДокумента`= 2 OR `idВидДокумента`= 3) AND (`Дата взятия` + interval 1  month < now()) AND `Дата возврата` is null) " +
                "OR((`idВидДокумента`= 6 OR `idВидДокумента`= 7) AND `Дата взятия` + interval 15  day < now() AND `Дата возврата` is null);";
        try {
            statement = dbConnection.createStatement();
            rs = statement.executeQuery(selectSQL);

            while(rs.next()){
                idOrder[size] = rs.getInt("idЗаказ");
                idReader[size] = rs.getInt("idЧитатель");
                size++;
            }
            for(int i = 0; i < size; i++) {
                reader = getReader(idReader[i]);
                order = getOrder(idOrder[i]);
                card = new LibraryCard();
                card.addOrder(order);
                card.setReader(reader);
                card.setId(Integer.toString(idReader[i]));
                list.add(card);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    public Map<Date, Integer> getCountOrders(){
        Map<Date, Integer> map = new LinkedHashMap<>();
        String selectSQL = "SELECT date_format(`Дата взятия`,'%Y-%m-%d'), COUNT(date_format(`Дата взятия`,'%Y-%m-%d')) FROM `database`.заказ\n" +
                "     GROUP BY date_format(`Дата взятия`,'%Y-%m-%d');";
        try {
            statement = dbConnection.createStatement();
            rs = statement.executeQuery(selectSQL);
            while(rs.next()){
                Timestamp timestamp = rs.getTimestamp("date_format(`Дата взятия`,'%Y-%m-%d')");
                Date date = new Date(timestamp.getTime());
                int count = rs.getInt("COUNT(date_format(`Дата взятия`,'%Y-%m-%d'))");
                map.put(date,count);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return map;
    }

    public Map<Date, Integer> getCountReturner(){
        Map<Date, Integer> map = new LinkedHashMap<>();
        int []idOrder = new int[1000];
        int size = 0;
        String selectSQL = "SELECT date_format(`Дата возврата`,'%Y-%m-%d'), COUNT(date_format(`Дата возврата`,'%Y-%m-%d')) FROM `database`.заказ\n" +
                "WHERE `Дата возврата` is not null GROUP BY date_format(`Дата возврата`,'%Y-%m-%d');";
        try {
            statement = dbConnection.createStatement();
            rs = statement.executeQuery(selectSQL);
            while(rs.next()){
                Timestamp timestamp = rs.getTimestamp("date_format(`Дата возврата`,'%Y-%m-%d')");
                Date date = new java.util.Date(timestamp.getTime());
                int count = rs.getInt("COUNT(date_format(`Дата возврата`,'%Y-%m-%d'))");
                map.put(date,count);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return map;
    }

}
