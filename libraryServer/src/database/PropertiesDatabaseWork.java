package database;

import library.*;

import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Босс on 27.10.2016.
 */
public interface PropertiesDatabaseWork {
    Book getBook(int idBook);
    Reader getReader(int idReader);
    Order getOrder(int idOrder);
    LibraryCard getLibraryCard(int idReader);
    KindOfBook getKind(int idKind);
    int getIdKindBook(String kindBook);
    Publisher getPublisher(int idPublisher);
    String getDepartment(int idDepartment);

    Map<Integer,Book> getAllBooks();
    Map<Integer,Book> getAllBooks(String kindFind, String strFind);
    LinkedList<Order> getAllOrders();
    Map<Integer,Order> getAllOrders(int idReader);
    Map<Integer,Reader> getAllReaders();
    String getBookDescription(int idBook);

    void addOrder(int idReader, int idBook) throws SQLException;
    void addReader(Reader reader) throws SQLException;
    void removeReader(int idReader) throws SQLException;
    void updateOrder(int idOrder) throws SQLException;

    Map<Integer,Reader> findReaders(String strFind);
    Map<Integer,Book> findBook(String kindBook);
    Map<Integer,Book> findBook(String kindBook, String kindFind, String strFind);

    List<LibraryCard> listLateReaders();
    Map<Date, Integer> getCountOrders();
    Map<Date, Integer> getCountReturner();

}
