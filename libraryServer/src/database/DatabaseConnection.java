package database;

import java.sql.*;

/**
 * Created by Босс on 02.10.2016.
 */
public abstract class DatabaseConnection {
    private static final String url = "jdbc:mysql://localhost:3306/database?useSSL=false";
    private static final String user = "root";
    private static final String password = "root";

    protected static Connection dbConnection;
    protected static Statement statement;
    protected static ResultSet rs;

    public DatabaseConnection(){
        try {
            dbConnection = DriverManager.getConnection(url, user,password);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void close() throws SQLException {
        if (statement != null) {
            statement.close();
        }
            dbConnection.close();
    }

    public abstract boolean checkEntry(String login, String password, boolean isAdmin);
    public abstract int checkRegistration(String id, String phone,String login, String password);
    public abstract boolean checkId(String id) throws SQLException;
    public abstract boolean checkLogin(String login, String password,String id) throws SQLException;
    public abstract boolean changePassword(String login, String oldPassword, String newPassword);
    public abstract int findIdReader(String login) throws SQLException;
}
