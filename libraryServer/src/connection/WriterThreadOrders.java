package connection;


import library.Order;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Босс on 11.10.2016.
 */
public class WriterThreadOrders extends Thread {

    List<Order> list;

    private String fileName = "C:/Users/Босс/IdeaProjects/kursach/orders.txt";

    public WriterThreadOrders() {
    }

    public void setList(LinkedList<Order> list){this.list = list;}

    public void run(){
        String fileString = "";
        String lineSeparator = System.getProperty("line.separator");
        for(int i = 0; i< list.size();i++) {
            fileString += "Заказ " + (i+1) + ":" + list.get(i).toString()+lineSeparator;
        }
        try(BufferedWriter writer = new BufferedWriter(new FileWriter(fileName)))
        {
            writer.write(fileString);
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
        System.out.println("Информация о заказах записана в файл " + fileName);
    }
}
