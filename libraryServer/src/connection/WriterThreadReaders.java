package connection;

import library.Reader;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Босс on 17.11.2016.
 */
public class WriterThreadReaders extends Thread {

    List<Reader> list;

    private String fileName = "C:/Users/Босс/IdeaProjects/kursach/readers.txt";

    public void setList(LinkedList<Reader> list){this.list = list;}

    public void run(){
        String fileString = "";
        String lineSeparator = System.getProperty("line.separator");
        for(int i = 0; i< list.size();i++) {
            fileString += list.get(i).toString()+lineSeparator;
        }
        try(BufferedWriter writer = new BufferedWriter(new FileWriter(fileName)))
        {
            writer.write(fileString);
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
        System.out.println("Информация о читателях записана в файл " + fileName);
    }
}

