package connection;

import database.DatabaseWork;
import library.LibraryCard;
import library.Reader;

import java.awt.*;
import java.io.*;
import java.net.Socket;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.Map;

/**
 * Created by Босс on 18.10.2016.
 */
public class ThreadServer extends Thread {
    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;
    private int count;
    private ObjectInputStream inputStream;
    private ObjectOutputStream outputStream;
    private DatabaseWork db;

    public ThreadServer(Socket s, int count) throws IOException {
        this.count = count;
        socket = s;
        db = new DatabaseWork();
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket
                .getOutputStream())), true);
        inputStream = new ObjectInputStream(socket.getInputStream());
        outputStream = new ObjectOutputStream(socket.getOutputStream());
        start();
    }

    public void run() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        try {
            System.out.println("Подключился " + count + "-й клиент IP: " + socket.getInetAddress().toString().substring(1) + " " + dateFormat.format(new Date()).toString());
            String client = count + "-й клиент";
            boolean entry = false;
            int registration;
            String login = null, password, admin = "false", id, phone;
            while (true) {
                int checkEntry = Integer.parseInt(in.readLine());
                switch (checkEntry) {
                    case 1:
                        boolean isAdmin = false;
                        login = in.readLine();
                        password = in.readLine();
                        admin = in.readLine();
                        if (admin.equals("true"))
                            isAdmin = true;
                        entry = db.checkEntry(login, password, isAdmin);
                        out.println(entry);
                        if (entry) {
                            if (admin.equals("false")) {
                                System.out.println(client + ":Вход в качестве пользователя " + dateFormat.format(new Date()).toString());
                                userConnection(login, client);
                            } else {
                                System.out.println(client + ":Вход в качестве администратора " + dateFormat.format(new Date()).toString());
                                adminConnection(client);
                            }
                        }
                        break;
                    case 2:
                        id = in.readLine();
                        phone = in.readLine();
                        login = in.readLine();
                        password = in.readLine();
                        registration = db.checkRegistration(id, phone, login, password);
                        System.out.println(client + ":Регистрация пользователя " + dateFormat.format(new Date()).toString());
                        out.println(registration);
                        break;
                    default:
                        break;
                }
            }
        } catch (IOException e) {
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                System.out.println("Отключился " + count + "-й клиент " + dateFormat.format(new Date()).toString());
                socket.close();
            } catch (IOException e) {
                System.err.println("Socket not closed");
            }
        }
    }


    void adminConnection(String client) throws IOException, SQLException, ClassNotFoundException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        outputStream.writeObject(db.getAllReaders());
        int choice = 1;
        String strFind;
        int strRemove, indexReader, indexOrder;
        Reader reader;
        while (choice != 0) {
            choice = Integer.parseInt(in.readLine());
            switch (choice) {
                case 1:
                    outputStream.writeObject(db.getAllReaders());
                    break;
                case 2:
                    strFind = in.readLine();
                    outputStream.writeObject(db.findReaders(strFind));
                    System.out.println(client + ":Поиск читателей " + dateFormat.format(new Date()).toString());
                    break;
                case 3:
                    indexReader = Integer.parseInt(in.readLine());
                    outputStream.writeObject(db.getAllOrders(indexReader));
                    System.out.println(client + ":Просмотр заказов " + dateFormat.format(new Date()).toString());
                    break;
                case 4:
                    strRemove = Integer.parseInt(in.readLine());
                    db.removeReader(strRemove);
                    outputStream.writeObject(db.getAllReaders());
                    System.out.println(client + ":Удаление читателя " + dateFormat.format(new Date()).toString());
                    writeReadersInFile();
                    break;
                case 5:
                    reader = (Reader) inputStream.readObject();
                    db.addReader(reader);
                    outputStream.writeObject(db.getAllReaders());
                    System.out.println(client + ":Добавление читателя " + dateFormat.format(new Date()).toString());
                    writeReadersInFile();
                    break;
                case 6:
                    indexReader = Integer.parseInt(in.readLine());
                    indexOrder = Integer.parseInt(in.readLine());
                    db.updateOrder(indexOrder);
                    outputStream.writeObject(db.getAllOrders(indexReader));
                    System.out.println(client + ":Возврат заказа " + dateFormat.format(new Date()).toString());
                    writeOrdersInFile();
                    break;
                case 7:
                    outputStream.writeObject(db.listLateReaders());
                    System.out.println(client + ":Просмотр должников " + dateFormat.format(new Date()).toString());
                    break;
                case 8:
                    outputStream.writeObject(db.getCountOrders());
                    outputStream.writeObject(db.getCountReturner());
                    System.out.println(client + ":Просмотр статистики работы " + dateFormat.format(new Date()).toString());
                    break;
                case 9:
                    openFile("C:/Users/Босс/IdeaProjects/kursach/orders.txt");
                    System.out.println(client + ":Просмотр отчета о заказах " + dateFormat.format(new Date()).toString());
                    break;
                case 10:
                    openFile("C:/Users/Босс/IdeaProjects/kursach/readers.txt");
                    System.out.println(client + ":Просмотр отчета о читателях " + dateFormat.format(new Date()).toString());
                    break;
                case 0:
                    break;

            }
        }
    }

    void userConnection(String login, String client) throws IOException, SQLException, ClassNotFoundException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        LibraryCard card = db.getLibraryCard(db.findIdReader(login));
        outputStream.writeObject(card);
        int choice = 1, indexBook;
        String newPassword, oldPassword, kindBook, kindFind, strFind;
        boolean changePassword;
        while (choice != 0) {
            choice = Integer.parseInt(in.readLine());
            switch (choice) {
                case 1:
                    card = db.getLibraryCard(db.findIdReader(login));
                    outputStream.writeObject(card);
                    break;
                case 2:
                    oldPassword = in.readLine();
                    newPassword = in.readLine();
                    changePassword = db.changePassword(login, oldPassword, newPassword);
                    out.println(changePassword);
                    System.out.println(client + ":Изменение пароля " + dateFormat.format(new Date()).toString());
                    break;
                case 3:
                    outputStream.writeObject(db.getAllBooks());
                    System.out.println(client + ":Просмотр списка книг " + dateFormat.format(new Date()).toString());
                    break;
                case 4:
                    kindBook = in.readLine();
                    kindFind = in.readLine();
                    strFind = in.readLine();
                    if (kindBook.equals("Все")) {
                        if (strFind.equals(""))
                            outputStream.writeObject(db.getAllBooks());
                        else
                            outputStream.writeObject(db.getAllBooks(kindFind, strFind));
                    } else {
                        if (strFind.equals(""))
                            outputStream.writeObject(db.findBook(kindBook));
                        else
                            outputStream.writeObject(db.findBook(kindBook, kindFind, strFind));
                    }
                    System.out.println(client + ":Поиск книг " + dateFormat.format(new Date()).toString());
                    break;
                case 5:
                    indexBook = Integer.parseInt(in.readLine());
                    db.addOrder(db.findIdReader(login), indexBook);
                    outputStream.writeObject(db.getAllBooks());
                    System.out.println(client + ":Добавление заказа " + dateFormat.format(new Date()).toString());
                    writeOrdersInFile();
                    break;
                case 6:
                    indexBook = Integer.parseInt(in.readLine());
                    out.println(db.getBookDescription(indexBook));
                    System.out.println(client + ":Просмотр краткого описания " + dateFormat.format(new Date()).toString());
                    break;
                case 7:
                    openFile("C:/Users/Босс/IdeaProjects/kursach/kniga.pdf");
                    System.out.println(client + ":Просмотр книги " + dateFormat.format(new Date()).toString());
                    break;
                case 0:
                    break;
            }

        }
    }



    public void writeOrdersInFile(){
        WriterThreadOrders threadOrders = new WriterThreadOrders();
        threadOrders.setList(db.getAllOrders());
        threadOrders.start();
    }

    public void writeReadersInFile(){
        WriterThreadReaders threadReaders = new WriterThreadReaders();
        LinkedList<Reader> listReaders = new LinkedList<>();
        Map<Integer,Reader> mapReader = db.getAllReaders();
        for (Map.Entry set : mapReader.entrySet()) {
            listReaders.add((Reader)set.getValue());
        }
        threadReaders.setList(listReaders);
        threadReaders.start();
    }

    public void openFile(String filename){
        Desktop desktop = null;
        if(Desktop.isDesktopSupported()){
            desktop = Desktop.getDesktop();
        }
        try {
            desktop.open(new File(filename));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
